package com.nb.crawler.html;

/**
 * Created by cuibo on 15/1/14.
 */
public interface XPathCallback<T> {
    T call(Element element, String xpath);
}
