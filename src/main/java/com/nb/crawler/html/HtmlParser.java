package com.nb.crawler.html;

import com.nb.crawler.util.HttpUtils;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import java.util.List;

/**
 * Created by cuibo on 15/1/14.
 */
public class HtmlParser {

    private HtmlParser() {}

    public static Element parse(String url) {
        String html = HttpUtils.getHtmlQuietly(url);

        HtmlCleaner htmlCleaner = new HtmlCleaner();
        TagNode doc = htmlCleaner.clean(html);

        return new Element(doc);
    }


}
