package com.nb.crawler.jd;

public class JDProductListResponse {
	private String result;
	private String moduleText;
	private String moduleInstanceId;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getModuleText() {
		return moduleText;
	}

	public void setModuleText(String moduleText) {
		this.moduleText = moduleText;
	}

	public String getModuleInstanceId() {
		return moduleInstanceId;
	}

	public void setModuleInstanceId(String moduleInstanceId) {
		this.moduleInstanceId = moduleInstanceId;
	}
}
