package com.nb.crawler.tmall;

import com.nb.crawler.html.Element;
import com.nb.crawler.html.HtmlParser;

/**
 * Created by cuibo on 15/1/14.
 */
public class TMallTask implements Runnable {
    private String url;

    public TMallTask(String url) {
        this.url = url;
    }

    @Override
    public void run() {
//        System.out.println(url);
        Element doc = HtmlParser.parse(url);

        String title = doc.getTextByXPath("//*[@id=\"J_DetailMeta\"]/div[1]/div[1]/div/div[1]/h1");
        System.out.println(title);
        String sales = doc.getTextByXPath("//*[@id=\"J_DetailMeta\"]/div[1]/div[1]/div/ul/li[1]/div/span[2]");
        System.out.println(sales);
        String originPrice = doc.getTextByXPath("//*[@id=\"J_ItemRates\"]/div/span[2]");
        System.out.println(originPrice);
    }
}
