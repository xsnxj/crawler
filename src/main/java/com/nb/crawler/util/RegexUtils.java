package com.nb.crawler.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by cuibo on 15/1/11.
 */
public class RegexUtils {
    public static List<String> matchAll(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);

        List<String> result = new ArrayList<>();

        if (matcher.find()) {
            result.add(matcher.group());
        }

        return result;
    }

    public static String match(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);

        if (matcher.find()) {
            return matcher.group();
        }

        return null;
    }

    public static void main(String[] args) {
        String s = match("New Balance旗舰店/NB  男女 经典复古鞋 运动中性 MRT580“侦探系列“ MRT580GK/灰色 41.5", "[A-Z]*\\d+[A-Z]*");

        System.out.println(s);
    }
}
