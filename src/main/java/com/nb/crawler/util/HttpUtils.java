package com.nb.crawler.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * Created by cuibo on 15/1/14.
 */
public class HttpUtils {
    public static String getHtml(String url) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();

        HttpResponse response = client.execute(new HttpGet(url));
        String html = EntityUtils.toString(response.getEntity());

        return html;
    }

    public static String getHtmlQuietly(String url) {
        try {
            return getHtml(url);
        } catch (IOException e) {
            //ignore
        }
        return null;
    }
}
